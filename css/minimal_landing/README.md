# minimal landing

There is a header, content, and footer but nothing else. The page is setup with `flexbox`, and `js` handles the dark / light mode toggle.

<br>
<img src="./assets/images/project.png">
